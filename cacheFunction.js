const cacheFuntion = (cacheCallbackFunction) => {

    const dummyNumberArray = [3,5,7,5,11,15,13]

    const cacheObject = {}

    const innerCacheFunction = () => {
        
        let resultFromCAllbackFuntion = null
        for (let number of dummyNumberArray) {

            if (cacheObject[number] !== undefined) {
    
                return cacheObject
    
            } else {
    
                cacheObject[number] = number * number
    
                resultFromCAllbackFuntion = cacheCallbackFunction(number,number*number)
    
            }
        }
        return resultFromCAllbackFuntion
    }

    return innerCacheFunction

}

module.exports = cacheFuntion
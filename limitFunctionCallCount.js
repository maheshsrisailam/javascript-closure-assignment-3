const limitFunctionCallCount = (callbackFunction,functionCallsLimitValue) => {
    let functionCallsCount = undefined
    const innerLimitFunctionCallCount = () => {
        if (functionCallsLimitValue !== undefined) {
            for (let count = 1 ; count <= functionCallsLimitValue ; count++) {

                if (count <= functionCallsLimitValue ) {
        
                    functionCallsCount = callbackFunction()
        
                }else{
                    
                    functionCallsCount = null
                }
            }
            return functionCallsCount
        } else {
            return null
        }
    }
    return innerLimitFunctionCallCount
}

module.exports = limitFunctionCallCount
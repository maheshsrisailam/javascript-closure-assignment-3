const counterFactory = () => {
    let counter = 20
    const innerCounterFactory = () => {
        return {
            increment: () => counter += 1,
            decrement: () => counter -= 1
        }
    }
    return innerCounterFactory
}

module.exports = counterFactory


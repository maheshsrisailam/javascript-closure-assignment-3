const counterFactory = require("../counterFactory")

const objectReturnedFromCounterFactory = counterFactory()

console.log("Actual Counter Value is 20")
console.log(`Incremented Counter Value: ${objectReturnedFromCounterFactory().increment()}`)
console.log(`Decremented Counter Value: ${objectReturnedFromCounterFactory().decrement()}`)
const limitFunctionCallCount = require('../limitFunctionCallCount')

let functionCallsCount = 0

const callbackFunction = () => {
    
    return functionCallsCount += 1
}

const funtionCallsCountReturnedFuntion = limitFunctionCallCount(callbackFunction,functionCallsLimitValue = 10)

console.log(`Invoked count value of the callback function : ${funtionCallsCountReturnedFuntion()}`)
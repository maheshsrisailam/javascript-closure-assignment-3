const cacheFunction = require('../cacheFunction')

const dummyObject = {}

const cacheCallbackFunction = (objectKey,objectValue) => {
    dummyObject[objectKey] = objectValue
    return dummyObject
}

const resultOfCacheFunction = cacheFunction(cacheCallbackFunction)
console.log(resultOfCacheFunction())